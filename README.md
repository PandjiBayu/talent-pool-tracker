# Talent Pool Tracker

## Package Installation

Run `npm install` or `npm i` in the directory where you clone this repository in your terminal to install all packages.

### Setup

- This app runs in port 3000 by default, but you can create your own port e.g. `5000` in file root `index.js`.
- Create a `.env.development` file inside your first folder, and add the MongoDB URI e.g. `mongodb+srv://projectDev:346hbGhg4565dsdfsJSD3r@cluster0.bxxvb.mongodb.net/talent_pool_tracker_dev`
- Create a `.env.test` file as well and use `mongodb+srv://projectTester:346hbGhg4565dsdfsJSD3r@cluster0.bxxvb.mongodb.net/talent_pool_tracker_test` for MongoDB URI testing.

### Run Program

- Run the program using `npm run dev` in your terminal.
- Run the program using `npm run test` for testing process.

## API Doc

Here the API documentation: https://documenter.getpostman.com/view/16564668/UUy7bjiP

## Webserver Link

Check live server here: https://talent-tracker-app.herokuapp.com/
