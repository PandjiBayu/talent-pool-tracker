const request = require("supertest");
const app = require("../index");
const { pic } = require("../models");

let picId = "";

beforeAll(async () => {
  const picTest = await pic.create({
    username: "test",
    fullname: "picTest",
    photo: "document/pict-test.jpg",
    address: "test no 1",
  });

  picId = picTest._id.toString();
});

describe("Create PIC", () => {
  it("Success", async () => {
    const response = await request(app).post("/pic").send({
      username: "test",
      fullname: "picTest",
      photo: "document/pict-test.jpg",
      address: "test no 1",
    });

    expect(response.statusCode).toEqual(201);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("message");
    expect(response.body).toHaveProperty("dataPIC");
  });

  it("Invalid Username", async () => {
    const response = await request(app).post("/pic").send({
      username: "",
      fullname: "picTest",
      photo: "document/pict-test.jpg",
      address: "test no 1",
    });

    expect(response.statusCode).toEqual(400);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });

  it("Fullname cannot be empty", async () => {
    const response = await request(app).post("/pic").send({
      username: "test",
      fullname: "",
      photo: "document/pict-test.jpg",
      address: "test no 1",
    });

    expect(response.statusCode).toEqual(400);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });

  it("Address cannot be empty", async () => {
    const response = await request(app).post("/pic").send({
      username: "test",
      fullname: "picTest",
      photo: "document/pict-test.jpg",
      address: "",
    });

    expect(response.statusCode).toEqual(400);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });

  it("Wrong Route", async () => {
    const response = await request(app).post("/picS").send({
      username: "test",
      fullname: "picTest",
      photo: "document/pict-test.jpg",
      address: "test no 1",
    });

    expect(response.statusCode).toEqual(404);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });
});

describe("Update PIC", () => {
  it("Success", async () => {
    const response = await request(app).put(`/pic/${picId}`).send({
      username: "test",
      fullname: "picTest",
      photo: "document/pict-test.jpg",
      address: "test no 1",
    });

    expect(response.statusCode).toEqual(201);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("message");
    expect(response.body).toHaveProperty("dataPIC");
  });

  it("Invalid Username", async () => {
    const response = await request(app).put(`/pic/${picId}`).send({
      username: "",
      fullname: "picTest",
      photo: "document/pict-test.jpg",
      address: "test no 1",
    });

    expect(response.statusCode).toEqual(400);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });

  it("Fullname cannot be empty", async () => {
    const response = await request(app).put(`/pic/${picId}`).send({
      username: "test",
      fullname: "",
      photo: "document/pict-test.jpg",
      address: "test no 1",
    });

    expect(response.statusCode).toEqual(400);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });

  it("Address cannot be empty", async () => {
    const response = await request(app).put(`/pic/${picId}`).send({
      username: "test",
      fullname: "picTest",
      photo: "document/pict-test.jpg",
      address: "",
    });

    expect(response.statusCode).toEqual(400);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });

  it("Invalid PIC ID", async () => {
    const response = await request(app).put(`/pic/${picId}test`).send({
      username: "test",
      fullname: "picTest",
      photo: "document/pict-test.jpg",
      address: "test no 1",
    });

    expect(response.statusCode).toEqual(400);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });

  it("Wrong Route", async () => {
    const response = await request(app).put(`/picS/${picId}`).send({
      username: "test",
      fullname: "picTest",
      photo: "document/pict-test.jpg",
      address: "test no 1",
    });

    expect(response.statusCode).toEqual(404);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });
});

describe("Update Photo PIC", () => {
  it("Success", async () => {
    const response = await request(app).put(`/pic/photo/${picId}`).send({
      photo: "document/pict-test.jpg",
    });

    expect(response.statusCode).toEqual(201);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("message");
    expect(response.body).toHaveProperty("dataPIC");
  });
});

describe("Get All PIC", () => {
  it("Success", async () => {
    const response = await request(app).get("/pic");

    expect(response.statusCode).toEqual(200);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("dataPIC");
  });

  it("Success by Page", async () => {
    const response = await request(app).get("/pic").query({ page: 1 });

    expect(response.statusCode).toEqual(200);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("dataPIC");
  });

  it("Success by Limit", async () => {
    const response = await request(app).get("/pic").query({ limit: 1 });

    expect(response.statusCode).toEqual(200);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("dataPIC");
  });

  it("Success by Limit and Page", async () => {
    const response = await request(app)
      .get("/pic")
      .query({ limit: 1, page: 1 });

    expect(response.statusCode).toEqual(200);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("dataPIC");
  });

  it("Limit and Page must be number", async () => {
    const response = await request(app)
      .get("/pic")
      .query({ limit: "test", page: "test" });

    expect(response.statusCode).toEqual(400);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });

  it("Wrong Route", async () => {
    const response = await request(app)
      .get("/picS")
      .query({ limit: 1, page: 1 });

    expect(response.statusCode).toEqual(404);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });
});

describe("Get Detail PIC", () => {
  it("Success", async () => {
    const response = await request(app).get(`/pic/${picId}`);

    expect(response.statusCode).toEqual(200);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("dataPIC");
  });

  it("Invalid Id PIC", async () => {
    const response = await request(app).get(`/pic/${picId}test`);

    expect(response.statusCode).toEqual(400);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });

  it("Wrong Route", async () => {
    const response = await request(app).get(`/picS/${picId}`);

    expect(response.statusCode).toEqual(404);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });
});

describe("Delete PIC", () => {
  it("Success", async () => {
    const response = await request(app).delete(`/pic/${picId}`);

    expect(response.statusCode).toEqual(200);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("message");
  });

  it("PIC not found", async () => {
    const response = await request(app).delete(`/pic/${picId}test`);

    expect(response.statusCode).toEqual(400);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });

  it("Wrong Route", async () => {
    const response = await request(app).delete(`/picS/${picId}`);

    expect(response.statusCode).toEqual(404);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });
});
