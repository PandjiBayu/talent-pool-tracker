const request = require("supertest");
const app = require("../index");
const { talent } = require("../models");

let talentId = "";

beforeAll(async () => {
  const talentTest = await talent.create({
    photo: "document/test-photo.jpg",
    education: ["sma", "kuliah"],
    username: "talentTest",
    fullname: "talent test",
    address: "test no 1",
    phone: "081236987243",
    experience: [
      {
        role: "role test",
        company: "company test",
        startDate: "2021-09-02",
        endDate: "2021-01-01",
      },
    ],
  });

  talentId = talentTest._id.toString();
});

describe("Create Talent", () => {
  it("Success", async () => {
    const response = await request(app)
      .post("/talent")
      .send({
        photo: "dokument/test-photo.jpg",
        education: ["test1", "test2"],
        username: "talentTest",
        fullname: "talent test",
        address: "test no 1",
        phone: "081236987243",
        experience: [
          {
            role: "role test",
            company: "company test",
            startDate: "2021-09-02",
            endDate: "2021-01-01",
          },
        ],
      });

    expect(response.statusCode).toEqual(201);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("message");
    expect(response.body).toHaveProperty("dataTalent");
  });

  it("Invalid Username", async () => {
    const response = await request(app)
      .post("/talent")
      .send({
        photo: "dokument/test-photo.jpg",
        education: ["test1", "test2"],
        username: "talent Test",
        fullname: "talent test",
        address: "test no 1",
        phone: "081236987243",
        experience: [
          {
            role: "role test",
            company: "company test",
            startDate: "2021-09-02",
            endDate: "2021-01-01",
          },
        ],
      });

    expect(response.statusCode).toEqual(400);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });

  it("Invalid Username Character", async () => {
    const response = await request(app)
      .post("/talent")
      .send({
        photo: "dokument/test-photo.jpg",
        education: ["test1", "test2"],
        username: "T",
        fullname: "talent test",
        address: "test no 1",
        phone: "081236987243",
        experience: [
          {
            role: "role test",
            company: "company test",
            startDate: "2021-09-02",
            endDate: "2021-01-01",
          },
        ],
      });

    expect(response.statusCode).toEqual(400);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });

  it("Fullname cannot be empty", async () => {
    const response = await request(app)
      .post("/talent")
      .send({
        photo: "dokument/test-photo.jpg",
        education: ["test1", "test2"],
        username: "talentTest",
        fullname: "",
        address: "test no 1",
        phone: "081236987243",
        experience: [
          {
            role: "role test",
            company: "company test",
            startDate: "2021-09-02",
            endDate: "2021-01-01",
          },
        ],
      });

    expect(response.statusCode).toEqual(400);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });

  it("Address cannot be empty", async () => {
    const response = await request(app)
      .post("/talent")
      .send({
        photo: "dokument/test-photo.jpg",
        education: ["test1", "test2"],
        username: "talentTest",
        fullname: "test Talent",
        address: "",
        phone: "081236987243",
        experience: [
          {
            role: "role test",
            company: "company test",
            startDate: "2021-09-02",
            endDate: "2021-01-01",
          },
        ],
      });

    expect(response.statusCode).toEqual(400);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });

  it("Phone cannot be empty", async () => {
    const response = await request(app)
      .post("/talent")
      .send({
        photo: "dokument/test-photo.jpg",
        education: ["test1", "test2"],
        username: "talentTest",
        fullname: "talent test",
        address: "test no 1",
        phone: "",
        experience: [
          {
            role: "role test",
            company: "company test",
            startDate: "2021-09-02",
            endDate: "2021-01-01",
          },
        ],
      });

    expect(response.statusCode).toEqual(400);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });

  it("Invalid startDate", async () => {
    const response = await request(app)
      .post("/talent")
      .send({
        photo: "dokument/test-photo.jpg",
        education: ["test1", "test2"],
        username: "talentTest",
        fullname: "talent test",
        address: "test no 1",
        phone: "081236987243",
        experience: [
          {
            role: "role test",
            company: "company test",
            startDate: "2021-09-test",
            endDate: "2021-01-01",
          },
        ],
      });

    expect(response.statusCode).toEqual(400);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });

  it("Invalid endDate", async () => {
    const response = await request(app)
      .post("/talent")
      .send({
        photo: "dokument/test-photo.jpg",
        education: ["test1", "test2"],
        username: "talentTest",
        fullname: "talent test",
        address: "test no 1",
        phone: "081236987243",
        experience: [
          {
            role: "role test",
            company: "company test",
            startDate: "2021-09-02",
            endDate: "2021-01-test",
          },
        ],
      });

    expect(response.statusCode).toEqual(400);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });

  it("Wrong Route", async () => {
    const response = await request(app)
      .post("/talentS")
      .send({
        photo: "dokument/test-photo.jpg",
        education: ["test1", "test2"],
        username: "talentTest",
        fullname: "talent test",
        address: "test no 1",
        phone: "081236987243",
        experience: [
          {
            role: "role test",
            company: "company test",
            startDate: "2021-09-02",
            endDate: "2021-01-01",
          },
        ],
      });

    expect(response.statusCode).toEqual(404);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });
});

describe("Update Talent", () => {
  it("Success", async () => {
    const response = await request(app)
      .put(`/talent/${talentId}`)
      .send({
        photo: "dokument/test-photo.jpg",
        education: ["test1", "test2"],
        username: "Test",
        fullname: "talent test",
        address: "test no 1",
        phone: "081236987243",
        experience: [
          {
            role: "role test",
            company: "company test",
            startDate: "2021-09-02",
            endDate: "2021-01-01",
          },
        ],
      });

    expect(response.statusCode).toEqual(201);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("message");
    expect(response.body).toHaveProperty("dataTalent");
  });

  it("Invalid Username", async () => {
    const response = await request(app)
      .put(`/talent/${talentId}`)
      .send({
        photo: "dokument/test-photo.jpg",
        education: ["test1", "test2"],
        username: "talent Test",
        fullname: "talent test",
        address: "test no 1",
        phone: "081236987243",
        experience: [
          {
            role: "role test",
            company: "company test",
            startDate: "2021-09-02",
            endDate: "2021-01-01",
          },
        ],
      });

    expect(response.statusCode).toEqual(400);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });

  it("Invalid Username Character", async () => {
    const response = await request(app)
      .put(`/talent/${talentId}`)
      .send({
        photo: "dokument/test-photo.jpg",
        education: ["test1", "test2"],
        username: "T",
        fullname: "talent test",
        address: "test no 1",
        phone: "081236987243",
        experience: [
          {
            role: "role test",
            company: "company test",
            startDate: "2021-09-02",
            endDate: "2021-01-01",
          },
        ],
      });

    expect(response.statusCode).toEqual(400);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });

  it("Fullname cannot be empty", async () => {
    const response = await request(app)
      .put(`/talent/${talentId}`)
      .send({
        photo: "dokument/test-photo.jpg",
        education: ["test1", "test2"],
        username: "talentTest",
        fullname: "",
        address: "test no 1",
        phone: "081236987243",
        experience: [
          {
            role: "role test",
            company: "company test",
            startDate: "2021-09-02",
            endDate: "2021-01-01",
          },
        ],
      });

    expect(response.statusCode).toEqual(400);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });

  it("Address cannot be empty", async () => {
    const response = await request(app)
      .put(`/talent/${talentId}`)
      .send({
        photo: "dokument/test-photo.jpg",
        education: ["test1", "test2"],
        username: "talentTest",
        fullname: "test Talent",
        address: "",
        phone: "081236987243",
        experience: [
          {
            role: "role test",
            company: "company test",
            startDate: "2021-09-02",
            endDate: "2021-01-01",
          },
        ],
      });

    expect(response.statusCode).toEqual(400);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });

  it("Phone cannot be empty", async () => {
    const response = await request(app)
      .put(`/talent/${talentId}`)
      .send({
        photo: "dokument/test-photo.jpg",
        education: ["test1", "test2"],
        username: "talentTest",
        fullname: "talent test",
        address: "test no 1",
        phone: "",
        experience: [
          {
            role: "role test",
            company: "company test",
            startDate: "2021-09-02",
            endDate: "2021-01-01",
          },
        ],
      });

    expect(response.statusCode).toEqual(400);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });

  it("Invalid startDate", async () => {
    const response = await request(app)
      .put(`/talent/${talentId}`)
      .send({
        photo: "dokument/test-photo.jpg",
        education: ["test1", "test2"],
        username: "talentTest",
        fullname: "talent test",
        address: "test no 1",
        phone: "081236987243",
        experience: [
          {
            role: "role test",
            company: "company test",
            startDate: "2021-09-test",
            endDate: "2021-01-01",
          },
        ],
      });

    expect(response.statusCode).toEqual(400);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });

  it("Invalid endDate", async () => {
    const response = await request(app)
      .put(`/talent/${talentId}`)
      .send({
        photo: "dokument/test-photo.jpg",
        education: ["test1", "test2"],
        username: "talentTest",
        fullname: "talent test",
        address: "test no 1",
        phone: "081236987243",
        experience: [
          {
            role: "role test",
            company: "company test",
            startDate: "2021-09-02",
            endDate: "2021-01-test",
          },
        ],
      });

    expect(response.statusCode).toEqual(400);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });

  it("Invalid Talent ID", async () => {
    const response = await request(app)
      .put(`/talent/${talentId}test`)
      .send({
        photo: "dokument/test-photo.jpg",
        education: ["test1", "test2"],
        username: "talentTest",
        fullname: "talent test",
        address: "test no 1",
        phone: "081236987243",
        experience: [
          {
            role: "role test",
            company: "company test",
            startDate: "2021-09-02",
            endDate: "2021-01-01",
          },
        ],
      });

    expect(response.statusCode).toEqual(400);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });

  it("Wrong Route", async () => {
    const response = await request(app)
      .put(`/talentS/${talentId}`)
      .send({
        photo: "dokument/test-photo.jpg",
        education: ["test1", "test2"],
        username: "talentTest",
        fullname: "talent test",
        address: "test no 1",
        phone: "081236987243",
        experience: [
          {
            role: "role test",
            company: "company test",
            startDate: "2021-09-02",
            endDate: "2021-01-01",
          },
        ],
      });

    expect(response.statusCode).toEqual(404);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });
});

describe("Update Photo Talent", () => {
  it("Success", async () => {
    const response = await request(app).put(`/talent/photo/${talentId}`).send({
      photo: "dokument/test-photo.jpg",
    });

    expect(response.statusCode).toEqual(201);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("message");
    expect(response.body).toHaveProperty("dataTalent");
  });
});

describe("Get All Talent", () => {
  it("Success", async () => {
    const response = await request(app).get("/talent");

    expect(response.statusCode).toEqual(200);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("dataTalent");
  });

  it("Success by Page", async () => {
    const response = await request(app).get("/talent").query({ page: 1 });

    expect(response.statusCode).toEqual(200);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("dataTalent");
  });

  it("Success by Limit", async () => {
    const response = await request(app).get("/talent").query({ limit: 1 });

    expect(response.statusCode).toEqual(200);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("dataTalent");
  });

  it("Success by Limit and Page", async () => {
    const response = await request(app)
      .get("/talent")
      .query({ limit: 1, page: 1 });

    expect(response.statusCode).toEqual(200);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("dataTalent");
  });

  it("Talent's Page and Limit must be number", async () => {
    const response = await request(app)
      .get("/talent")
      .query({ limit: "test", page: "test" });

    expect(response.statusCode).toEqual(400);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });

  it("Wrong Route", async () => {
    const response = await request(app)
      .get("/talentS")
      .query({ limit: 1, page: 1 });

    expect(response.statusCode).toEqual(404);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });
});

describe("Get Detail Talent", () => {
  it("Success", async () => {
    const response = await request(app).get(`/talent/${talentId}`);

    expect(response.statusCode).toEqual(200);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("dataTalent");
  });

  it("Invalid Id Talent", async () => {
    const response = await request(app).get(`/talent/${talentId}test`);

    expect(response.statusCode).toEqual(400);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });

  it("Wrong Route", async () => {
    const response = await request(app).get(`/talentS/${talentId}`);

    expect(response.statusCode).toEqual(404);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });
});

describe("Delete Talent", () => {
  it("Success", async () => {
    const response = await request(app).delete(`/talent/${talentId}`);

    expect(response.statusCode).toEqual(200);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("message");
  });

  it("Talent not found", async () => {
    const response = await request(app).delete(`/talent/${talentId}test`);

    expect(response.statusCode).toEqual(400);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });

  it("Wrong Route", async () => {
    const response = await request(app).delete(`/talentS/${talentId}`);

    expect(response.statusCode).toEqual(404);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });
});
