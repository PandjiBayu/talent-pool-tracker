const request = require("supertest");
const app = require("../index");
const { company } = require("../models");

let companyId = "";

beforeAll(async () => {
  const companyTest = await company.create({
    name: "companyTest",
    photo: "document/company-test.png",
    address: "test no 1",
    website: "test.com",
    phone: "03453611",
    jobVacancy: [
      {
        roleOpen: ["roleOpen1 unit testing", "roleOpen2 unit testing"],
        requirement: ["requirement1 unit testing", "requirement2 unit testing"],
        description: "This is unit testing description ",
        dateCreated: "2021-10-01",
        hiringStatus: true,
      },
    ],
  });

  companyId = companyTest._id.toString();
});

describe("Create Company", () => {
  it("Success", async () => {
    const response = await request(app)
      .post("/company")
      .send({
        name: "companyTest",
        photo: "document/company-test.png",
        address: "test no 1",
        website: "test.com",
        phone: "03453611",
        jobVacancy: [
          {
            roleOpen: ["roleOpen1 unit testing", "roleOpen2 unit testing"],
            requirement: [
              "requirement1 unit testing",
              "requirement2 unit testing",
            ],
            description: "This is unit testing description ",
            dateCreated: "2021-10-01",
            hiringStatus: true,
          },
        ],
      });

    expect(response.statusCode).toEqual(201);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("message");
    expect(response.body).toHaveProperty("dataCompany");
  });

  it("Name cannot be empty", async () => {
    const response = await request(app)
      .post("/company")
      .send({
        name: "",
        photo: "document/company-test.png",
        address: "test no 1",
        website: "test.com",
        phone: "03453611",
        jobVacancy: [
          {
            roleOpen: ["roleOpen1 unit testing", "roleOpen2 unit testing"],
            requirement: [
              "requirement1 unit testing",
              "requirement2 unit testing",
            ],
            description: "This is unit testing description ",
            dateCreated: "2021-10-01",
            hiringStatus: true,
          },
        ],
      });

    expect(response.statusCode).toEqual(400);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });

  it("Address cannot be empty", async () => {
    const response = await request(app)
      .post("/company")
      .send({
        name: "companyTest",
        photo: "document/company-test.png",
        address: "",
        website: "test.com",
        phone: "03453611",
        jobVacancy: [
          {
            roleOpen: ["roleOpen1 unit testing", "roleOpen2 unit testing"],
            requirement: [
              "requirement1 unit testing",
              "requirement2 unit testing",
            ],
            description: "This is unit testing description ",
            dateCreated: "2021-10-01",
            hiringStatus: true,
          },
        ],
      });

    expect(response.statusCode).toEqual(400);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });

  it("Website cannot be empty", async () => {
    const response = await request(app)
      .post("/company")
      .send({
        name: "companyTest",
        photo: "document/company-test.png",
        address: "test no 1",
        website: "",
        phone: "03453611",
        jobVacancy: [
          {
            roleOpen: ["roleOpen1 unit testing", "roleOpen2 unit testing"],
            requirement: [
              "requirement1 unit testing",
              "requirement2 unit testing",
            ],
            description: "This is unit testing description ",
            dateCreated: "2021-10-01",
            hiringStatus: true,
          },
        ],
      });

    expect(response.statusCode).toEqual(400);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });

  it("Phone cannot be empty", async () => {
    const response = await request(app)
      .post("/company")
      .send({
        name: "companyTest",
        photo: "document/company-test.png",
        address: "test no 1",
        website: "test.com",
        phone: "",
        jobVacancy: [
          {
            roleOpen: ["roleOpen1 unit testing", "roleOpen2 unit testing"],
            requirement: [
              "requirement1 unit testing",
              "requirement2 unit testing",
            ],
            description: "This is unit testing description ",
            dateCreated: "2021-10-01",
            hiringStatus: true,
          },
        ],
      });

    expect(response.statusCode).toEqual(400);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });

  it("Invalid dateCreated", async () => {
    const response = await request(app)
      .post("/company")
      .send({
        name: "companyTest",
        photo: "document/company-test.png",
        address: "test no 1",
        website: "test.com",
        phone: "03453611",
        jobVacancy: [
          {
            roleOpen: ["roleOpen1 unit testing", "roleOpen2 unit testing"],
            requirement: [
              "requirement1 unit testing",
              "requirement2 unit testing",
            ],
            description: "This is unit testing description ",
            dateCreated: "2021-10-test",
            hiringStatus: true,
          },
        ],
      });

    expect(response.statusCode).toEqual(400);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });

  it("Wrong Route", async () => {
    const response = await request(app)
      .post("/companyS")
      .send({
        name: "companyTest",
        photo: "document/company-test.png",
        address: "test no 1",
        website: "test.com",
        phone: "03453611",
        jobVacancy: [
          {
            roleOpen: ["roleOpen1 unit testing", "roleOpen2 unit testing"],
            requirement: [
              "requirement1 unit testing",
              "requirement2 unit testing",
            ],
            description: "This is unit testing description ",
            dateCreated: "2021-10-01",
            hiringStatus: true,
          },
        ],
      });

    expect(response.statusCode).toEqual(404);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });
});

describe("Update Company", () => {
  it("Success", async () => {
    const response = await request(app)
      .put(`/company/${companyId}`)
      .send({
        name: "companyTest",
        photo: "document/company-test.png",
        address: "test no 1",
        website: "test.com",
        phone: "03453611",
        jobVacancy: [
          {
            roleOpen: ["roleOpen1 unit testing", "roleOpen2 unit testing"],
            requirement: [
              "requirement1 unit testing",
              "requirement2 unit testing",
            ],
            description: "This is unit testing description ",
            dateCreated: "2021-10-01",
            hiringStatus: true,
          },
        ],
      });

    expect(response.statusCode).toEqual(201);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("message");
    expect(response.body).toHaveProperty("dataCompany");
  });

  it("Name cannot be empty", async () => {
    const response = await request(app)
      .put(`/company/${companyId}`)
      .send({
        name: "",
        photo: "document/company-test.png",
        address: "test no 1",
        website: "test.com",
        phone: "03453611",
        jobVacancy: [
          {
            roleOpen: ["roleOpen1 unit testing", "roleOpen2 unit testing"],
            requirement: [
              "requirement1 unit testing",
              "requirement2 unit testing",
            ],
            description: "This is unit testing description ",
            dateCreated: "2021-10-01",
            hiringStatus: true,
          },
        ],
      });

    expect(response.statusCode).toEqual(400);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });

  it("Address cannot be empty", async () => {
    const response = await request(app)
      .put(`/company/${companyId}`)
      .send({
        name: "companyTest",
        photo: "document/company-test.png",
        address: "",
        website: "test.com",
        phone: "03453611",
        jobVacancy: [
          {
            roleOpen: ["roleOpen1 unit testing", "roleOpen2 unit testing"],
            requirement: [
              "requirement1 unit testing",
              "requirement2 unit testing",
            ],
            description: "This is unit testing description ",
            dateCreated: "2021-10-01",
            hiringStatus: true,
          },
        ],
      });

    expect(response.statusCode).toEqual(400);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });

  it("Website cannot be empty", async () => {
    const response = await request(app)
      .put(`/company/${companyId}`)
      .send({
        name: "companyTest",
        photo: "document/company-test.png",
        address: "test no 1",
        website: "",
        phone: "03453611",
        jobVacancy: [
          {
            roleOpen: ["roleOpen1 unit testing", "roleOpen2 unit testing"],
            requirement: [
              "requirement1 unit testing",
              "requirement2 unit testing",
            ],
            description: "This is unit testing description ",
            dateCreated: "2021-10-01",
            hiringStatus: true,
          },
        ],
      });

    expect(response.statusCode).toEqual(400);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });

  it("Phone cannot be empty", async () => {
    const response = await request(app)
      .put(`/company/${companyId}`)
      .send({
        name: "companyTest",
        photo: "document/company-test.png",
        address: "test no 1",
        website: "test.com",
        phone: "",
        jobVacancy: [
          {
            roleOpen: ["roleOpen1 unit testing", "roleOpen2 unit testing"],
            requirement: [
              "requirement1 unit testing",
              "requirement2 unit testing",
            ],
            description: "This is unit testing description ",
            dateCreated: "2021-10-01",
            hiringStatus: true,
          },
        ],
      });

    expect(response.statusCode).toEqual(400);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });

  it("Invalid dateCreated", async () => {
    const response = await request(app)
      .put(`/company/${companyId}`)
      .send({
        name: "companyTest",
        photo: "document/company-test.png",
        address: "test no 1",
        website: "test.com",
        phone: "03453611",
        jobVacancy: [
          {
            roleOpen: ["roleOpen1 unit testing", "roleOpen2 unit testing"],
            requirement: [
              "requirement1 unit testing",
              "requirement2 unit testing",
            ],
            description: "This is unit testing description ",
            dateCreated: "2021-10-test",
            hiringStatus: true,
          },
        ],
      });

    expect(response.statusCode).toEqual(400);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });

  it("Invalid Company ID", async () => {
    const response = await request(app)
      .put(`/company/${companyId}test`)
      .send({
        name: "companyTest",
        photo: "document/company-test.png",
        address: "test no 1",
        website: "test.com",
        phone: "03453611",
        jobVacancy: [
          {
            roleOpen: ["roleOpen1 unit testing", "roleOpen2 unit testing"],
            requirement: [
              "requirement1 unit testing",
              "requirement2 unit testing",
            ],
            description: "This is unit testing description ",
            dateCreated: "2021-10-01",
            hiringStatus: true,
          },
        ],
      });

    expect(response.statusCode).toEqual(400);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });

  it("Wrong Route", async () => {
    const response = await request(app)
      .put(`/companyS/${companyId}`)
      .send({
        name: "companyTest",
        photo: "document/company-test.png",
        address: "test no 1",
        website: "test.com",
        phone: "03453611",
        jobVacancy: [
          {
            roleOpen: ["roleOpen1 unit testing", "roleOpen2 unit testing"],
            requirement: [
              "requirement1 unit testing",
              "requirement2 unit testing",
            ],
            description: "This is unit testing description ",
            dateCreated: "2021-10-01",
            hiringStatus: true,
          },
        ],
      });

    expect(response.statusCode).toEqual(404);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });
});

describe("Update Photo Company", () => {
  it("Success", async () => {
    const response = await request(app)
      .put(`/company/photo/${companyId}`)
      .send({
        photo: "document/company-test.png",
      });

    expect(response.statusCode).toEqual(201);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("message");
    expect(response.body).toHaveProperty("dataCompany");
  });
});

describe("Get All Company", () => {
  it("Success", async () => {
    const response = await request(app).get("/company");

    expect(response.statusCode).toEqual(200);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("dataCompany");
  });

  it("Success by Page", async () => {
    const response = await request(app).get("/company").query({ page: 1 });

    expect(response.statusCode).toEqual(200);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("dataCompany");
  });

  it("Success by Limit", async () => {
    const response = await request(app).get("/company").query({ limit: 1 });

    expect(response.statusCode).toEqual(200);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("dataCompany");
  });

  it("Success by Limit and Page", async () => {
    const response = await request(app)
      .get("/company")
      .query({ limit: 1, page: 1 });

    expect(response.statusCode).toEqual(200);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("dataCompany");
  });

  it("Limit and Page must be number", async () => {
    const response = await request(app)
      .get("/company")
      .query({ limit: "test", page: "test" });

    expect(response.statusCode).toEqual(400);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });

  it("Wrong Route", async () => {
    const response = await request(app)
      .get("/companyS")
      .query({ limit: 1, page: 1 });

    expect(response.statusCode).toEqual(404);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });
});

describe("Get Detail Company", () => {
  it("Success", async () => {
    const response = await request(app).get(`/company/${companyId}`);

    expect(response.statusCode).toEqual(200);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("dataCompany");
  });

  it("Invalid Id Company", async () => {
    const response = await request(app).get(`/company/${companyId}test`);

    expect(response.statusCode).toEqual(400);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });

  it("Wrong Route", async () => {
    const response = await request(app).get(`/companyS/${companyId}`);

    expect(response.statusCode).toEqual(404);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });
});

describe("Delete Company", () => {
  it("Success", async () => {
    const response = await request(app).delete(`/company/${companyId}`);

    expect(response.statusCode).toEqual(200);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("message");
  });

  it("Company not found", async () => {
    const response = await request(app).delete(`/company/${companyId}test`);

    expect(response.statusCode).toEqual(400);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });

  it("Wrong Route", async () => {
    const response = await request(app).delete(`/companyS/${companyId}`);

    expect(response.statusCode).toEqual(404);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });
});
