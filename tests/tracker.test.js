const request = require("supertest");
const app = require("../index");
const { tracker, talent, company, pic } = require("../models");

let talentId = "";
let companyId = "";
let picId = "";
let trackerId = "";

beforeAll(async () => {
  const talentTestTracker = await talent.create({
    photo: "document/test-photo.jpg",
    education: ["sma", "kuliah"],
    username: "talentTest",
    fullname: "talent test",
    address: "test no 1",
    phone: "081236987243",
    experience: [
      {
        role: "role test",
        company: "company test",
        startDate: "2021-09-02",
        endDate: "2021-01-01",
      },
    ],
  });

  const companyTestTracker = await company.create({
    name: "companyTest",
    photo: "document/company-test.png",
    address: "test no 1",
    website: "test.com",
    phone: "03453611",
    jobVacancy: [
      {
        roleOpen: ["roleOpen1 unit testing", "roleOpen2 unit testing"],
        requirement: ["requirement1 unit testing", "requirement2 unit testing"],
        description: "This is unit testing description ",
        dateCreated: "2021-10-01",
        hiringStatus: true,
      },
    ],
  });

  const picTestTracker = await pic.create({
    username: "test",
    fullname: "picTest",
    photo: "document/pict-test.jpg",
    address: "test no 1",
  });

  talentId = talentTestTracker._id;
  companyId = companyTestTracker._id;
  picId = picTestTracker._id;

  const trackerTest = await tracker.create({
    status: "Review",
    talents: talentId,
    companies: companyId,
    pics: picId,
  });

  trackerId = trackerTest._id.toString();
});

describe("Create Tracker", () => {
  it("Success", async () => {
    const response = await request(app).post("/tracker").send({
      status: "Review",
      talents: talentId,
      companies: companyId,
      pics: picId,
    });

    expect(response.statusCode).toEqual(201);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("message");
    expect(response.body).toHaveProperty("dataTracker");
  });

  it("Incorrect Status", async () => {
    const response = await request(app).post("/tracker").send({
      status: "",
      talents: talentId,
      companies: companyId,
      pics: picId,
    });

    expect(response.statusCode).toEqual(400);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });

  it("Invalid Talent ID", async () => {
    const response = await request(app)
      .post("/tracker")
      .send({
        status: "Review",
        talents: `${talentId}test`,
        companies: companyId,
        pics: picId,
      });

    expect(response.statusCode).toEqual(400);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });

  it("Invalid Company ID", async () => {
    const response = await request(app)
      .post("/tracker")
      .send({
        status: "Review",
        talents: talentId,
        companies: `${companyId}test`,
        pics: picId,
      });

    expect(response.statusCode).toEqual(400);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });

  it("Invalid PIC ID", async () => {
    const response = await request(app)
      .post("/tracker")
      .send({
        status: "Review",
        talents: talentId,
        companies: companyId,
        pics: `${picId}test`,
      });

    expect(response.statusCode).toEqual(400);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });

  it("Wrong Route", async () => {
    const response = await request(app).post("/trackerS").send({
      status: "Review",
      talents: talentId,
      companies: companyId,
      pics: picId,
    });

    expect(response.statusCode).toEqual(404);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });
});

describe("Update Tracker", () => {
  it("Success", async () => {
    const response = await request(app).put(`/tracker/${trackerId}`).send({
      status: "Review",
      talents: talentId,
      companies: companyId,
      pics: picId,
    });

    expect(response.statusCode).toEqual(201);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("message");
    expect(response.body).toHaveProperty("dataTracker");
  });

  it("Incorrect Status", async () => {
    const response = await request(app).put(`/tracker/${trackerId}`).send({
      status: "",
      talents: talentId,
      companies: companyId,
      pics: picId,
    });

    expect(response.statusCode).toEqual(400);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });

  it("Invalid Talent ID", async () => {
    const response = await request(app)
      .put(`/tracker/${trackerId}`)
      .send({
        status: "Review",
        talents: `${talentId}test`,
        companies: companyId,
        pics: picId,
      });

    expect(response.statusCode).toEqual(400);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });

  it("Invalid Company ID", async () => {
    const response = await request(app)
      .put(`/tracker/${trackerId}`)
      .send({
        status: "Review",
        talents: talentId,
        companies: `${companyId}test`,
        pics: picId,
      });

    expect(response.statusCode).toEqual(400);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });

  it("Invalid PIC ID", async () => {
    const response = await request(app)
      .put(`/tracker/${trackerId}`)
      .send({
        status: "Review",
        talents: talentId,
        companies: companyId,
        pics: `${picId}test`,
      });

    expect(response.statusCode).toEqual(400);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });

  it("Invalid Tracker ID", async () => {
    const response = await request(app).put(`/tracker/${trackerId}test`).send({
      status: "Review",
      talents: talentId,
      companies: companyId,
      pics: picId,
    });

    expect(response.statusCode).toEqual(400);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });
  it("Wrong Route", async () => {
    const response = await request(app).put(`/trackerS/${trackerId}`).send({
      status: "Review",
      talents: talentId,
      companies: companyId,
      pics: picId,
    });

    expect(response.statusCode).toEqual(404);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });
});

describe("Get All Tracker", () => {
  it("Success", async () => {
    const response = await request(app).get("/tracker");

    expect(response.statusCode).toEqual(200);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("dataTracker");
  });

  it("Success by Page", async () => {
    const response = await request(app).get("/tracker").query({ page: 1 });

    expect(response.statusCode).toEqual(200);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("dataTracker");
  });

  it("Success by Limit", async () => {
    const response = await request(app).get("/tracker").query({ limit: 1 });

    expect(response.statusCode).toEqual(200);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("dataTracker");
  });

  it("Success by Limit and Page", async () => {
    const response = await request(app)
      .get("/tracker")
      .query({ limit: 1, page: 1 });

    expect(response.statusCode).toEqual(200);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("dataTracker");
  });

  it("Limit and Page must be number", async () => {
    const response = await request(app)
      .get("/tracker")
      .query({ limit: "test", page: "test" });

    expect(response.statusCode).toEqual(400);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });

  it("Wrong Route", async () => {
    const response = await request(app)
      .get("/trackerS")
      .query({ limit: 1, page: 1 });

    expect(response.statusCode).toEqual(404);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });
});

describe("Get Tracker by Status", () => {
  it("Success", async () => {
    const response = await request(app).get("/tracker/status/Review");

    expect(response.statusCode).toEqual(200);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("dataTracker");
  });

  it("Data Tracker not found", async () => {
    const response = await request(app).get("/tracker/status/Evaluate");

    expect(response.statusCode).toEqual(404);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });

  it("Wrong Route", async () => {
    const response = await request(app).get("/trackerS/status/Review");

    expect(response.statusCode).toEqual(404);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });
});
