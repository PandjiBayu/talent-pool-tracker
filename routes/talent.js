const express = require("express");
const multer = require("multer");
const { talent_upload } = require("../cloudinary");
const upload = multer({ storage: talent_upload });

// Import validator
const {
  detailValidator,
  createAndUpdateValidator,
  queryValidator,
} = require("../middlewares/validators/talent");

// Import controller
const {
  getAllTalent,
  createTalent,
  GetDetailTalent,
  updateTalent,
  destroyTalent,
  updatePhoto,
} = require("../controllers/talent");

// Router
const router = express.Router();

// Make some routes
router
  .route("/")
  .get(queryValidator, getAllTalent)
  .post(createAndUpdateValidator, createTalent);

router
  .route("/:id")
  .get(detailValidator, GetDetailTalent)
  .put(detailValidator, createAndUpdateValidator, updateTalent)
  .delete(detailValidator, destroyTalent);

router.route("/photo/:id").put(upload.single("photo"), updatePhoto);
// Exports
module.exports = router;
