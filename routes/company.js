const express = require("express");
const multer = require("multer");
const { company_upload } = require("../cloudinary");
const upload = multer({ storage: company_upload });

// Import validator
const {
  createAndUpdateValidator,
  detailValidator,
  queryValidator,
} = require("../middlewares/validators/company");

// Import controller
const {
  getAllCompany,
  createCompany,
  GetDetailCompany,
  updateCompany,
  destroyCompany,
  updatePhoto,
} = require("../controllers/company");

// Router
const router = express.Router();

// Make some routes
router
  .route("/")
  .get(queryValidator, getAllCompany)
  .post(createAndUpdateValidator, createCompany);

router
  .route("/:id")
  .get(detailValidator, GetDetailCompany)
  .put(detailValidator, createAndUpdateValidator, updateCompany)
  .delete(detailValidator, destroyCompany);

router.route("/photo/:id").put(upload.single("photo"), updatePhoto);

// Exports
module.exports = router;
