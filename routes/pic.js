const express = require("express");
const multer = require("multer");
const { pic_upload } = require("../cloudinary");
const upload = multer({ storage: pic_upload });

// Import validator
const {
  detailValidator,
  createAndUpdateValidator,
  queryValidator,
} = require("../middlewares/validators/pic");

// Import controller
const {
  getAllPIC,
  createPIC,
  GetDetailPIC,
  updatePIC,
  destroyPIC,
  updatePhoto,
} = require("../controllers/pic");

// Router
const router = express.Router();

// Make some routes
router
  .route("/")
  .get(queryValidator, getAllPIC)
  .post(createAndUpdateValidator, createPIC);

router
  .route("/:id")
  .get(detailValidator, GetDetailPIC)
  .put(detailValidator, createAndUpdateValidator, updatePIC)
  .delete(detailValidator, destroyPIC);

router.route("/photo/:id").put(upload.single("photo"), updatePhoto);

// Exports
module.exports = router;
