const express = require("express");

// Import validator
const {
  detailValidator,
  createAndUpdateValidator,
  queryValidator,
} = require("../middlewares/validators/tracker");

// Import controller
const {
  createTracker,
  getDetailTrackerByTalent,
  getAllTracker,
  getTalentByTrackerStatus,
  updateTracker,
} = require("../controllers/tracker");

// Router
const router = express.Router();

// Make some routes
router
  .route("/")
  .get(queryValidator, getAllTracker)
  .post(createAndUpdateValidator, createTracker);

router.route("/status/:status").get(queryValidator, getTalentByTrackerStatus);

router
  .route("/:id")
  .get(detailValidator, getDetailTrackerByTalent)
  .put(detailValidator, createAndUpdateValidator, updateTracker);

// Exports
module.exports = router;
