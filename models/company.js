const mongoose = require("mongoose");
const mongooseDelete = require("mongoose-delete");

const companySchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
    },
    photo: {
      type: String,
      required: false,
    },
    address: {
      type: String,
      required: true,
    },
    website: {
      type: String,
      required: true,
    },
    phone: {
      type: Number,
      required: true,
    },
    jobVacancy: [
      {
        roleOpen: {
          type: [String],
          required: false,
        },
        requirement: {
          type: [String],
          required: false,
        },
        description: {
          type: String,
          required: false,
        },
        dateCreated: {
          type: Date,
          required: false,
        },
        hiringStatus: {
          type: Boolean,
          required: false,
          default: true,
        },
      },
    ],
  },
  {
    timestamps: {
      createdAt: "createdAt",
      updatedAt: "updateAt",
    },
    toObject: { getters: true, versionKey: false },
  }
);

companySchema.plugin(mongooseDelete, { overrideMethods: "all" });
module.exports = mongoose.model("company", companySchema);
