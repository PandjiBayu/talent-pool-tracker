require("dotenv").config({
  path: `.env.${process.env.NODE_ENV}`,
}); // Config environment
const mongoose = require("mongoose");

mongoose
  .connect(process.env.MONGO_URI, {
    useCreateIndex: true, // Enable unique
    useNewUrlParser: true, // Must be true
    useUnifiedTopology: true, // Must be true
    useFindAndModify: false, // to use updateOne and updateMany
  })
  .then(() => console.log("MongoDB Connected"))
  /* istanbul ignore next */
  .catch((error) => console.log(error));

exports.talent = require("./talent");
exports.company = require("./company");
exports.pic = require("./pic");
exports.tracker = require("./tracker");
