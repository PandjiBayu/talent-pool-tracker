const mongoose = require("mongoose");
const mongooseDelete = require("mongoose-delete");

const picSchema = new mongoose.Schema(
  {
    username: {
      type: String,
      required: true,
    },
    fullname: {
      type: String,
      required: true,
    },
    photo: {
      type: String,
      default:
        "https://i1.wp.com/jejuhydrofarms.com/wp-content/uploads/2020/05/blank-profile-picture-973460_1280.png?fit=300%2C300&ssl=1",
    },
    address: {
      type: String,
      required: true,
    },
  },
  {
    timestamps: {
      createdAt: "createdAt",
      updatedAt: "updateAt",
    },
    toObject: { getters: true, versionKey: false },
  }
);

picSchema.plugin(mongooseDelete, { overrideMethods: "all" });
module.exports = mongoose.model("pic", picSchema);
