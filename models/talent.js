const mongoose = require("mongoose");
const mongooseDelete = require("mongoose-delete");

const talentSchema = new mongoose.Schema(
  {
    photo: {
      type: String,
      default:
        "https://i1.wp.com/jejuhydrofarms.com/wp-content/uploads/2020/05/blank-profile-picture-973460_1280.png?fit=300%2C300&ssl=1",
    },
    username: {
      type: String,
      required: true,
    },
    fullname: {
      type: String,
      required: true,
    },
    education: {
      type: [String],
      required: true,
    },
    address: {
      type: String,
      required: true,
    },
    phone: {
      type: String,
      required: true,
    },
    experience: [
      {
        role: {
          type: String,
          required: false,
        },
        company: {
          type: String,
          required: false,
        },
        startDate: {
          type: Date,
          required: false,
        },
        endDate: {
          type: Date,
          required: false,
        },
      },
    ],
    trackers: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "tracker",
      },
    ],
  },
  {
    timestamps: {
      createdAt: "createdAt",
      updatedAt: "updateAt",
    },
    toObject: { getters: true, versionKey: false },
  }
);

talentSchema.plugin(mongooseDelete, { overrideMethods: "all" });
module.exports = mongoose.model("talent", talentSchema);
