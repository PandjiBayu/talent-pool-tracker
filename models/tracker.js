const mongoose = require("mongoose");

const trackerSchema = new mongoose.Schema(
  {
    status: {
      type: String,
      required: true,
      enum: [
        "Review",
        "HR Interview",
        "User Interview",
        "Offer",
        "Accepted",
        "Rejected",
      ],
      default: "Review",
    },
    talents: {
      type: mongoose.Schema.ObjectId,
      ref: "talent",
      required: true,
    },
    companies: {
      type: mongoose.Schema.ObjectId,
      ref: "company",
      required: true,
    },
    pics: {
      type: mongoose.Schema.ObjectId,
      ref: "pic",
      required: true,
    },
  },
  {
    timestamps: {
      createdAt: "createdAt",
      updatedAt: "updateAt",
    },
    toObject: { getters: true, versionKey: false },
  }
);

module.exports = mongoose.model("tracker", trackerSchema);
