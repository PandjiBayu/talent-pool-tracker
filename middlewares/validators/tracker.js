const validator = require("validator");
const mongoose = require("mongoose");

exports.detailValidator = async (req, res, next) => {
  try {
    if (!mongoose.Types.ObjectId.isValid(req.params.id)) {
      return next({ message: "Id is not valid", statusCode: 400 });
    }
    next();
  } catch (error) {
    next(error);
  }
};

exports.createAndUpdateValidator = async (req, res, next) => {
  try {
    const errorMessages = [];

    const validateStatus = [
      "Review",
      "HR Interview",
      "User Interview",
      "Offer",
      "Accepted",
      "Rejected",
    ];

    if (validator.isEmpty(req.body.status)) {
      errorMessages.push("Status cannot be empty");
    }

    if (!validateStatus.includes(req.body.status)) {
      errorMessages.push(`Status must be one of ${validateStatus}`);
    }

    if (!req.body.talents || !validator.isMongoId(req.body.talents)) {
      errorMessages.push("Talent Id is invalid");
    }

    if (!req.body.companies || !validator.isMongoId(req.body.companies)) {
      errorMessages.push("Company Id is invalid");
    }

    if (!req.body.pics || !validator.isMongoId(req.body.pics)) {
      errorMessages.push("PIC Id is invalid");
    }

    if (errorMessages.length > 0) {
      return next({ messages: errorMessages, statusCode: 400 });
    }

    next();
  } catch (error) {
    next(error);
  }
};

exports.queryValidator = async (req, res, next) => {
  try {
    const errorMessages = [];

    if (req.query.page) {
      if (!validator.isInt(req.query.page)) {
        errorMessages.push("Page request must be number");
      }
    }
    if (req.query.limit) {
      if (!validator.isInt(req.query.limit)) {
        errorMessages.push("Limit request must be number");
      }
    }

    if (errorMessages.length > 0) {
      return next({ statusCode: 400, messages: errorMessages });
    }

    next();
  } catch (error) {
    /* istanbul ignore next */
    next(error);
  }
};
