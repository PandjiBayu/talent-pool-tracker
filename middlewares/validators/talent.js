const validator = require("validator");
const mongoose = require("mongoose");

exports.detailValidator = async (req, res, next) => {
  try {
    if (!mongoose.Types.ObjectId.isValid(req.params.id)) {
      return next({ message: "Id is not valid", statusCode: 400 });
    }
    next();
  } catch (error) {
    next(error);
  }
};

exports.createAndUpdateValidator = async (req, res, next) => {
  try {
    const errorMessages = [];

    const validate = ["fullname", "education", "address", "phone"];

    validate.map((data) => {
      if (!req.body[data]) {
        errorMessages.push(`${data} cannot be empty`);
      }
    });

    if (req.body.username.includes(" ")) {
      errorMessages.push("space is not allowed for username");
    }

    if (validator.isEmpty(req.body.username) || req.body.username.length <= 3) {
      errorMessages.push(
        "username must be at least 4 characters and cannot be empty"
      );
    }

    if (req.body.experience) {
      for (let i = 0; i < req.body.experience.length; i++) {
        if (
          !validator.isEmpty(req.body.username) &&
          !validator.isDate(req.body.experience[i].startDate)
        ) {
          errorMessages.push(
            "startDate not valid! Please enter a date in YYYY-MM-DD format"
          );
        }
        if (
          !validator.isEmpty(req.body.username) &&
          !validator.isDate(req.body.experience[i].endDate)
        ) {
          errorMessages.push(
            "endDate not valid! Please enter a date in YYYY-MM-DD format"
          );
        }
      }
    }

    if (errorMessages.length > 0) {
      return next({ messages: errorMessages, statusCode: 400 });
    }

    next();
  } catch (error) {
    next(error);
  }
};
exports.queryValidator = async (req, res, next) => {
  try {
    const errorMessages = [];

    if (req.query.page) {
      if (!validator.isInt(req.query.page)) {
        errorMessages.push("Page request must be number");
      }
    }
    if (req.query.limit) {
      if (!validator.isInt(req.query.limit)) {
        errorMessages.push("Limit request must be number");
      }
    }

    if (errorMessages.length > 0) {
      return next({ statusCode: 400, messages: errorMessages });
    }

    next();
  } catch (error) {
    /* istanbul ignore next */
    next(error);
  }
};
