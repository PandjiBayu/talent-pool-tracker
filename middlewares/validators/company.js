const validator = require("validator");
const mongoose = require("mongoose");
// const { talent } = require("../../models");

exports.detailValidator = async (req, res, next) => {
  try {
    if (!mongoose.Types.ObjectId.isValid(req.params.id)) {
      return next({ message: "Id is not valid", statusCode: 400 });
    }
    next();
  } catch (error) {
    next(error);
  }
};

exports.createAndUpdateValidator = async (req, res, next) => {
  try {
    const errorMessages = [];

    const validate = ["name", "address", "website", "phone"];

    validate.map((data) => {
      if (!req.body[data]) {
        errorMessages.push(`${data} cannot be empty`);
      }
    });

    if (validator.isInt(req.body.name)) {
      errorMessages.push("Name cannot be integer");
    }

    if (!validator.isInt(req.body.phone)) {
      errorMessages.push("Phone mush be number only");
    }

    if (req.body.jobVacancy) {
      for (let i = 0; i < req.body.jobVacancy.length; i++) {
        if (
          !validator.isEmpty(req.body.name) &&
          !validator.isDate(req.body.jobVacancy[i].dateCreated)
        ) {
          errorMessages.push(
            "dateCerated not valid! Please enter a date in YYYY-MM-DD format"
          );
        }
      }
    }

    if (errorMessages.length > 0) {
      return next({ messages: errorMessages, statusCode: 400 });
    }

    if (req.file) {
      req.body.photo = req.file.path;
    }
    next();
  } catch (error) {
    next(error);
  }
};
exports.queryValidator = async (req, res, next) => {
  try {
    const errorMessages = [];

    if (req.query.page) {
      if (!validator.isInt(req.query.page)) {
        errorMessages.push("Page request must be number");
      }
    }
    if (req.query.limit) {
      if (!validator.isInt(req.query.limit)) {
        errorMessages.push("Limit request must be number");
      }
    }

    if (errorMessages.length > 0) {
      return next({ statusCode: 400, messages: errorMessages });
    }

    next();
  } catch (error) {
    /* istanbul ignore next */
    next(error);
  }
};
