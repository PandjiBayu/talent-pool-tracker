const { tracker, talent } = require("../models");

class Tracker {
  async createTracker(req, res, next) {
    try {
      let trackerId = [];

      const newData = await tracker.create(req.body);
      const findTalent = await talent.findById(req.body.talents);
      trackerId.push(newData._id);

      if (findTalent.trackers.length > 0) {
        findTalent.trackers.map((x) => {
          trackerId.push(x);
        });
      }

      const updateTalentTrackerId = await talent.findByIdAndUpdate(
        req.body.talents,
        {
          trackers: trackerId,
        }
      );

      const dataTracker = await tracker
        .findOne({ _id: newData._id })
        .populate("talents", "fullname")
        .populate("companies", ["name", "photo", "address", "website", "phone"])
        .populate("pics", ["username", "fullname", "photo", "address"]);

      res.status(201).json({ message: "Data Tracker created", dataTracker });
    } catch (error) {
      next(error);
    }
  }
  async getAllTracker(req, res, next) {
    try {
      const pageLimit = parseInt(req.query.limit) || 15;
      const currentPage = req.query.page || 1;

      const dataTracker = await tracker
        .find()
        .populate("talents", ["fullname", "photo", "address"])
        .populate("companies", ["name", "photo", "address", "website", "phone"])
        .populate("pics", ["username", "fullname", "photo", "address"])
        .skip(pageLimit * (currentPage - 1))
        .limit(pageLimit)
        .sort("-createdAt");

      if (dataTracker.length === 0) {
        return next({ message: "Data not found", statusCode: 404 });
      }

      res.status(200).json({ dataTracker });
    } catch (error) {
      next(error);
    }
  }
  async getTalentByTrackerStatus(req, res, next) {
    try {
      const getStatus = req.params.status;

      const dataTracker = await tracker
        .find({ status: getStatus })
        .populate({ path: "talents", select: "-deleted" })
        .populate({ path: "companies", select: "-deleted" })
        .populate({ path: "pics", select: "-deleted" });

      if (dataTracker.length === 0) {
        return next({ message: "Data not found", statusCode: 404 });
      }

      res.status(200).json({ dataTracker });
    } catch (error) {
      next(error);
    }
  }
  async getDetailTrackerByTalent(req, res, next) {
    try {
      const dataTracker = await talent.findById(req.params.id).populate({
        path: "trackers",
        select: "-talents",
        populate: {
          path: "companies pics",
          select: "-jobVacancy -deleted",
        },
      });

      if (!dataTracker) {
        return next({ message: "Data not found", statusCode: 404 });
      }
      res.status(200).json({ dataTracker });
    } catch (error) {
      next(error);
    }
  }
  async updateTracker(req, res, next) {
    try {
      const dataTracker = await tracker
        .findOneAndUpdate({ _id: req.params.id }, req.body, { new: true })
        .populate("talents", "fullname")
        .populate("companies", ["name", "photo", "address", "website", "phone"])
        .populate("pics", ["username", "fullname", "photo", "address"]);

      if (!dataTracker) {
        return next({ message: "Data not found", statusCode: 404 });
      }

      res.status(201).json({ message: "Data updated", dataTracker });
    } catch (error) {
      next(error);
    }
  }
}

module.exports = new Tracker();
