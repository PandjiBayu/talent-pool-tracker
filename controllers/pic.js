const { pic } = require("../models");

class PIC {
  async createPIC(req, res, next) {
    try {
      const dataPIC = await pic.create(req.body);

      res.status(201).json({ message: "Successfully create PIC", dataPIC });
    } catch (error) {
      next(error);
    }
  }

  async getAllPIC(req, res, next) {
    try {
      const pageLimit = parseInt(req.query.limit) || 15;
      const currentPage = req.query.page || 1;

      const dataPIC = await pic
        .find()
        .skip(pageLimit * (currentPage - 1))
        .limit(pageLimit)
        .sort("-createdAt");

      if (dataPIC.length === 0) {
        return next({ message: "PIC not found", statusCode: 404 });
      }

      res.status(200).json({ dataPIC });
    } catch (error) {
      next(error);
    }
  }

  async GetDetailPIC(req, res, next) {
    try {
      const dataPIC = await pic.findById(req.params.id);

      if (dataPIC.length === 0) {
        return next({ message: "PIC not found", statusCode: 404 });
      }

      res.status(200).json({ dataPIC });
    } catch (error) {
      next(error);
    }
  }

  async updatePIC(req, res, next) {
    try {
      const dataPIC = await pic.findOneAndUpdate(
        { _id: req.params.id },
        req.body,
        { new: true }
      );

      if (!dataPIC) {
        return next({ message: "PIC not found", statusCode: 404 });
      }

      res.status(201).json({ message: "Successfully update PIC", dataPIC });
    } catch (error) {
      next(error);
    }
  }

  async updatePhoto(req, res, next) {
    try {
      if (req.file) {
        req.body.photo = req.file.path;
      }

      let dataPIC = await pic.findOneAndUpdate(
        {
          _id: req.params.id,
        },
        req.body,
        {
          new: true,
        }
      );

      return res.status(201).json({
        message: `Photo ${dataPIC.username} is updated.`,
        dataPIC,
      });
    } catch (error) {
      next(error);
    }
  }

  async destroyPIC(req, res, next) {
    try {
      const dataPIC = await pic.deleteById({ _id: req.params.id });

      if (dataPIC.n === 0) {
        return next({ message: "PIC not found", statusCode: 404 });
      }

      res.status(200).json({ message: "PIC has been deleted" });
    } catch (error) {
      next(error);
    }
  }
}

module.exports = new PIC();
