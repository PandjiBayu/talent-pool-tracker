const { company, tracker } = require("../models");

class Company {
  async createCompany(req, res, next) {
    try {
      const dataCompany = await company.create(req.body);

      res
        .status(201)
        .json({ message: "Successfully create Company", dataCompany });
    } catch (error) {
      next(error);
    }
  }

  async getAllCompany(req, res, next) {
    try {
      const pageLimit = parseInt(req.query.limit) || 15;
      const currentPage = req.query.page || 1;

      const dataCompany = await company
        .find()
        .skip(pageLimit * (currentPage - 1))
        .limit(pageLimit)
        .sort("-createdAt");

      if (dataCompany.length === 0) {
        return next({ message: "Company not found", statusCode: 404 });
      }

      res.status(200).json({ dataCompany });
    } catch (error) {
      next(error);
    }
  }

  async GetDetailCompany(req, res, next) {
    try {
      const dataCompany = await company.findById(req.params.id);

      if (dataCompany.length === 0) {
        return next({ message: "Company not found", statusCode: 404 });
      }

      res.status(200).json({ dataCompany });
    } catch (error) {
      next(error);
    }
  }

  async updateCompany(req, res, next) {
    try {
      const dataCompany = await company.findOneAndUpdate(
        { _id: req.params.id },
        req.body,
        { new: true }
      );

      if (!dataCompany) {
        return next({ message: "company not found", statusCode: 404 });
      }

      res
        .status(201)
        .json({ message: "Successfully update Company", dataCompany });
    } catch (error) {
      next(error);
    }
  }

  async updatePhoto(req, res, next) {
    try {
      if (req.file) {
        req.body.photo = req.file.path;
      }

      let dataCompany = await company.findOneAndUpdate(
        {
          _id: req.params.id,
        },
        req.body,
        {
          new: true,
        }
      );

      return res.status(201).json({
        message: `Photo ${dataCompany.name} is updated.`,
        dataCompany,
      });
    } catch (error) {
      next(error);
    }
  }

  async destroyCompany(req, res, next) {
    try {
      await tracker.deleteMany({ companies: req.params.id });
      const dataCompany = await company.deleteById({ _id: req.params.id });

      if (dataCompany.n === 0) {
        return next({ message: "company not found", statusCode: 404 });
      }

      res.status(200).json({ message: "Company has been deleted" });
    } catch (error) {
      next(error);
    }
  }
}

module.exports = new Company();
