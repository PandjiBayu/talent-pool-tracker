const { talent, tracker } = require("../models");

class Talent {
  async createTalent(req, res, next) {
    try {
      const dataTalent = await talent.create(req.body);

      res
        .status(201)
        .json({ message: "Successfully create Talent", dataTalent });
    } catch (error) {
      next(error);
    }
  }

  async getAllTalent(req, res, next) {
    try {
      const pageLimit = parseInt(req.query.limit) || 15;
      const currentPage = req.query.page || 1;

      const dataTalent = await talent
        .find()
        .skip(pageLimit * (currentPage - 1))
        .limit(pageLimit)
        .sort("-createdAt");

      if (dataTalent.length === 0) {
        return next({ message: "Talent not found", statusCode: 404 });
      }

      res.status(200).json({ dataTalent });
    } catch (error) {
      next(error);
    }
  }

  async GetDetailTalent(req, res, next) {
    try {
      const dataTalent = await talent.findById(req.params.id);

      if (dataTalent.length === 0) {
        return next({ message: "Talent not found", statusCode: 404 });
      }

      res.status(200).json({ dataTalent });
    } catch (error) {
      next(error);
    }
  }

  async updateTalent(req, res, next) {
    try {
      const dataTalent = await talent.findOneAndUpdate(
        { _id: req.params.id },
        req.body,
        { new: true }
      );

      if (!dataTalent) {
        return next({ message: "Talent not found", statusCode: 404 });
      }

      res
        .status(201)
        .json({ message: "Successfully update Talent", dataTalent });
    } catch (error) {
      next(error);
    }
  }

  async updatePhoto(req, res, next) {
    try {
      if (req.file) {
        req.body.photo = req.file.path;
      }

      let dataTalent = await talent.findOneAndUpdate(
        {
          _id: req.params.id,
        },
        req.body,
        {
          new: true,
        }
      );

      return res.status(201).json({
        message: `Photo ${dataTalent.username} is updated.`,
        dataTalent,
      });
    } catch (error) {
      next(error);
    }
  }

  async destroyTalent(req, res, next) {
    try {
      await tracker.deleteMany({ talents: req.params.id });
      const dataTalent = await talent.deleteById({ _id: req.params.id });

      if (dataTalent.n === 0) {
        return next({ message: "Talent not found", statusCode: 404 });
      }

      res.status(200).json({ message: "Talent has been deleted" });
    } catch (error) {
      next(error);
    }
  }
}

module.exports = new Talent();
