const cloudinary = require("cloudinary").v2;
const { CloudinaryStorage } = require("multer-storage-cloudinary");

cloudinary.config({
  cloud_name: process.env.CLOUDINARY_CLOUD_NAME,
  api_key: process.env.CLOUDINARY_API_KEY,
  api_secret: process.env.CLOUDINARY_API_SECRET,
});

const talent_upload = new CloudinaryStorage({
  cloudinary,
  params: {
    folder: "Talents",
    allowedFormats: ["jpeg", "png", "jpg"],
  },
});

const company_upload = new CloudinaryStorage({
  cloudinary,
  params: {
    folder: "Companies",
    allowedFormats: ["jpeg", "png", "jpg"],
  },
});

const pic_upload = new CloudinaryStorage({
  cloudinary,
  params: {
    folder: "PIC",
    allowedFormats: ["jpeg", "png", "jpg"],
  },
});

module.exports = { cloudinary, talent_upload, company_upload, pic_upload };
